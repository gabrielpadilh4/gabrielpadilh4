## Hi 👋

Would you like to find me ?

[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/gabriel-padilha/)](https://www.linkedin.com/in/gabriel-padilha/)
[![Instagram Badge](https://img.shields.io/badge/-Instagram-red?style=flat-square&logo=Instagram&logoColor=white&link=https://www.instagram.com/gabriel.padilh4/)](https://www.instagram.com/gabriel.padilh4/)
[![Gmail Badge](https://img.shields.io/badge/-Gmail-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:gabrielpadilhasantos@gmail.com)](mailto:gabrielpadilhasantos@gmail.com)

[![Visitors](https://visitor-badge.glitch.me/badge?page_id=gabrielpadilh4)](https://github.com/gabrielpadilh4)





